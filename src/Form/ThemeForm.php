<?php

namespace Drupal\tv\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements the RefreshingForm form controller.
 *
 *
 * @see \Drupal\Core\Form\FormBase
 */
class ThemeForm extends FormBase {

    /**
     * Build the theme form.
     *
     * A build form method constructs an array that defines how markup and
     * other form elements are included in an HTML form.
     *
     * @param array              $form
     *   Default form array structure.
     * @param FormStateInterface $form_state
     *   Object containing current form state.
     *
     * @return array
     *   The render array defining the elements of the form.
     */
    public function buildForm(array $form, FormStateInterface $form_state) {

        $form['type'] = [
            '#type'     => 'select',
            '#title'    => $this->t('Theme'),
            '#options'  => [
                'normal' => $this->t('Normal'),
                'happy'  => $this->t('Happy'),
                'sad'    => $this->t('Sad'),
            ],
            '#required' => TRUE,
        ];

        $form['title'] = [
            '#type'     => 'textfield',
            '#title'    => $this->t('Why change theme?'),
            '#required' => TRUE,
        ];

        // Group submit handlers in an actions element with a key of "actions" so
        // that it gets styled correctly, and so that other modules may add actions
        // to the form. This is not required, but is convention.
        $form['actions'] = [
            '#type' => 'actions',
        ];

        // Add a submit button that handles the submission of the form.
        $form['actions']['submit'] = [
            '#type'  => 'submit',
            '#value' => $this->t('Change'),
        ];

        return $form;
    }

    /**
     * Getter method for Form ID.
     *
     * The form ID is used in implementations of hook_form_alter() to allow other
     * modules to alter the render array built by this form controller.  it must
     * be unique site wide. It normally starts with the providing module's name.
     *
     * @return string
     *   The unique ID of the form defined by this class.
     */
    public function getFormId() {
        return 'entekhab_tv_theme_form';
    }

    /**
     * Implements form validation.
     *
     * The validateForm method is the default method called to validate input on
     * a form.
     *
     * @param array              $form
     *   The render array of the currently built form.
     * @param FormStateInterface $form_state
     *   Object describing the current state of the form.
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        $title = $form_state->getValue('title');
        if (!strlen($title)) {
            // Set an error for the form element with a key of "title".
            $form_state->setErrorByName('title', $this->t('The title is required.'));
        }
        $type = $form_state->getValue('type');
        if (!strlen($type)) {
            // Set an error for the form element with a key of "title".
            $form_state->setErrorByName('type', $this->t('The tv theme is required'));
        }
    }

    /**
     * Implements a form submit handler.
     *
     * The submitForm method is the default method called for any submit elements.
     *
     * @param array              $form
     *   The render array of the currently built form.
     * @param FormStateInterface $form_state
     *   Object describing the current state of the form.
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $title = $form_state->getValue('title');
        $type = $form_state->getValue('type');
        $user = $this->currentUser();

        $db = \Drupal::database();
        $db->insert('tv_theme')->fields(array(
            'title' => $title,
            'type'   => $type,
            'time'  => REQUEST_TIME,
            'uid'   => $user->id(),
        ))->execute();
        drupal_set_message(t('Tv will be change theme because %title.', ['%title' => $title]));
    }

}
