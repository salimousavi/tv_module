<?php

namespace Drupal\tv\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

class TvController {

    public function theme() {
        $db = \Drupal::database();
        $result = $db->select('tv_theme', 'e')->fields('e')->orderBy('id', 'DESC')->execute()->fetchObject();

        if (!$result) {
            return new JsonResponse('normal');
        }

        return new JsonResponse($result->type);
    }
}